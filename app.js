//Server Dependencies
const express = require("express")
const mongoose = require("mongoose")
require("dotenv").config()

//Import module
const taskRoutes = require("./routes/taskRoutes")

//MongoDB Connection
mongoose.connect(`mongodb+srv://admin:${process.env.MONGODB_PASSWORD}@wdc028-course-booking.sevh8po.mongodb.net/todo-db?retryWrites=true&w=majority`,{
	useNewUrlParser: true,
	useUnifiedTopology: true
})

//MongoDB Connection Listener
let db = mongoose.connection
db.on("error", () => console.log("Something went wrong"))
db.once("open", () => console.log("Connected to MongoDB!"))

//Server Setup
const app = express()
const port = 30001

//Middlewar 
app.use(express.json())
app.use(express.urlencoded({extended:true}))


//Routes
app.use("/tasks", taskRoutes)


//Server Listening
app.listen(port, () => console.log(`Server running at localhost: ${port}`))
