const mongoose = require("mongoose")

const task_schema = new mongoose.Schema({
	name: String,
	status: {
		type: String,
		default: "Pending"
	}
})


//Converts the js file as MODULE
module.exports = mongoose.model("Task", task_schema)