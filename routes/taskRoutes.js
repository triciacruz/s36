const express = require("express")
const router = express.Router()
const TaskController = require("../controllers/TaskController")

//Get Specific Task
router.get("/:id", (request, response)=>{
	TaskController.getTask(request.params.id).then(result => response.send(result))
})


//Get all task
router.get("/", (request, response) =>{
	TaskController.getAll().then(result => response.send(result))
})

//Create new task
router.post("/", (request, response) =>{
	TaskController.createTask(request.body).then(result => response.send(result))
})


//Update existing task
router.put("/:id", (request, response) =>{
	TaskController.updateTask(request.params.id, request.body).then(result => response.send(result))
})


//Delete existing task
router.delete("/:id", (request, response) =>{
	TaskController.deleteTask(request.params.id, request.body).then(result => response.send(result))
})


//Converts the js file as MODULE
module.exports = router